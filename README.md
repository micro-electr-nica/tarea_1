# Tarea 1: Verificación de contadores
## Universidad de Costa Rica
### Escuela de Ingeniería Eléctrica
#### IE-0411: Microelectrónica
##### Emmanuel Rivel Montero. B65868

## Última revisión: 
Último commit realizado el: 16/09/20

## Abstract
La siguiente tarea se basa en la realización de pruebas a 3 descripciones dadas en verilog, sintetizadas, para realizar una labor similar a la de un verificador. El estudiante debe generar las pruebas requeridas para encontrar las respectivas fallas en la definición de los módulos dados. Las pruebas fueron realizadas en Ubuntu 18.04.4, con Icarus Verilog 10.1.

### Instrucciones de ejecución
Para la ejecución general de la verificación, basta con **ejecutar el comando:**
```bash
make
```
**En la raíz del repositorio**, este comando llama al makefile que se encuentra en esta ubicación, ejecuta la compilación de las pruebas que requieren ser compiladas y muestra los resultados en GTKWave. Finalmente se abre el editor "gedit" con el log file que contiene el registro de las pruebas corridas y sus respectivos errores.

