`include "counter_tb.v"

module tb_top;

    wire rco_A, rco_B, rco_C, load_A, load_B, load_C, enable, reset, load;
    wire [3:0] Q_A, Q_B, Q_C;
    wire [1:0] mode;
    wire [3:0] D;

    counter_tb tb (
        .enable (enable),
        .reset (reset), 
        .mode (mode),
        .load (load),
        .D (D)
    );

endmodule