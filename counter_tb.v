`include "clock.v"
`include "scoreboard.v"
`include "contadorA.v"
`include "contadorB.v"
`include "contadorC.v"


module counter_tb(enable, reset, mode, D, load);
    
    output reg enable, reset, load;
    output reg [1:0] mode;
    output reg [3:0] D;
    
    `include "driver.v"
    `include "checker.v"
    
    parameter ITERATIONS = 10;
    integer log;

    wire [3:0] sb_Q, Q_A, Q_B, Q_C;
    wire sb_rco, sb_load, rco_A, rco_B, rco_C, load_A, load_B, load_C, clk;
    
    initial begin
        $dumpfile("test.vcd");
        $dumpvars(0);
    
        log = $fopen("tb.log");
    
        $fdisplay(log, "time=%5d, Simulation Start\n", $time);
        $fdisplay(log, "time=%5d, Starting Reset", $time);
        drv_init();
        $fdisplay(log, "time=%5d, Reset Completed\n\n", $time);


        /*              FIXED VALUES TEST             */
        $fdisplay(log, "time=%5d, Starting FIXED Test", $time);
        fork 
            fixed_counter(ITERATIONS);
            checker(ITERATIONS);
        join
        $fdisplay(log, "time=%5d, FIXED VALUES Test Completed\n\n", $time);



        /*              RESET/ENABLE TEST             */
        $fdisplay(log, "time=%5d, Starting RESET/ENABLE Test", $time);
        fork
            reset_breaker();
            checker(ITERATIONS);
        join
        $fdisplay(log, "time=%5d, RESET/ENABLE Test Completed\n\n", $time);


        /*              RAMDOM TEST             */
        $fdisplay(log, "time=%5d, Starting RANDOM Test", $time);
        fork
            rand_counter(ITERATIONS);  
            checker(ITERATIONS);
        join
        $fdisplay(log, "time=%5d, RANDOM Test Completed\n\n", $time);


        $fdisplay(log, "time=%5d, Simulation Completed", $time);
        $fclose(log);
        #200 $finish;
    end

    clk_mod clock (
        .clock (clk)
    );

    scoreboard sb(
        .enable (enable),
        .clk (clk),
        .reset (reset),
        .mode (mode),
        .D (D),
        .Q (sb_Q),
        .rco (sb_rco),
        .load (sb_load)
    );

    counterA dutA (
        .enable (enable),
        .clk (clk),
        .reset (reset),
        .mode (mode),
        .D (D),
        .load (load_A),
        .rco (rco_A),
        .Q (Q_A)
    );

    counterB dutB (
        .enable (enable),
        .clk (clk),
        .reset (reset),
        .mode (mode),
        .D (D),
        .load (load_B),
        .rco (rco_B),
        .Q (Q_B)
    );

    counterC dutC (
        .enable (enable),
        .clk (clk),
        .reset (reset),
        .mode (mode),
        .D (D),
        .load (load_C),
        .rco (rco_C),
        .Q (Q_C)
    );

endmodule