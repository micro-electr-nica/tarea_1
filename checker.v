task checker;

input integer iteration;

repeat (iteration) @ (posedge clk) begin
    if ({sb.Q, sb.rco, sb.load} == {Q_A, rco_A, load_A}) begin
      $fdisplay(log, "PASSED counter A on mode %b.", mode);
    end
    
    if ({sb.Q, sb.rco, sb.load} == {Q_B, rco_B, load_B}) begin
      $fdisplay(log, "PASSED counter B on mode %b.", mode);
    end

    if ({sb.Q, sb.rco, sb.load} == {Q_C, rco_C, load_C}) begin
      $fdisplay(log, "PASSED counter C on mode %b.", mode);
    end

    else begin
      if ({sb.Q, sb.rco, sb.load} == {Q_A, rco_A, load_A}) begin
        $fdisplay(log, "Time=%.0f Error in dutA:\n\tQ=%b, rco=%b, load=%b\n\tscoreboard:\n\tQ=%b, rco=%b, load=%b\n", $time, Q_A, rco_A, load_A, sb_Q, sb_rco, sb_load);
      end
      if ({sb.Q, sb.rco, sb.load} == {Q_B, rco_B, load_B}) begin
        $fdisplay(log, "Time=%.0f Error in dutB:\n\tQ=%b, rco=%b, load=%b\n\tscoreboard:\n\tQ=%b, rco=%b, load=%b\n", $time, Q_B, rco_B, load_B, sb_Q, sb_rco, sb_load);
      end
      if ({sb.Q, sb.rco, sb.load} == {Q_C, rco_C, load_C}) begin
        $fdisplay(log, "Time=%.0f Error in dutC:\n\tQ=%b, rco=%b, load=%b\n\tscoreboard:\n\tQ=%b, rco=%b, load=%b\n", $time, Q_C, rco_C, load_C, sb_Q, sb_rco, sb_load);
      end
    end
  end
endtask
